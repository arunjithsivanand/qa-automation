package gleac.pages;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.net.URI;
import java.util.List;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.paulhammant.ngwebdriver.ByAngular;
import com.paulhammant.ngwebdriver.ByAngularModel;

import gleac.base.TestBase;
import gleac.util.Lib;

public class SignupPage extends TestBase{



	@FindBy(xpath ="//input[@placeholder='Enter First Name']")
	WebElement FirstName;
	@FindBy(xpath ="//input[@placeholder='Enter Last Name']")
	WebElement LastName;
	@FindBy(xpath ="//input[@placeholder='Enter Email']")
	WebElement EmailID;
	@FindBy(xpath ="//input[@placeholder='Enter Password']")
	WebElement Password;
	@FindBy(xpath ="//input[@placeholder='Confirm Password']")
	WebElement Confirmpwd;
	@FindBy(xpath ="//input[@type='checkbox']")
	WebElement checkTOS;
	@FindBy(xpath ="//button/span[contains(text(),'Sign Up')]")
	WebElement signbt;


	@FindBy(xpath ="//*[@class='login-form ng-dirty ng-touched ng-valid']//button/span[contains(text(),'Sign Up')]")
	WebElement Signupbtn;



	
	@FindBy(xpath ="//*[@ng-reflect-name='code_1']")
	
	WebElement box1;

	@FindBy(xpath ="(//*[@class='ng-star-inserted'])[2]")
	WebElement verifybtn;






	
	private String s;
	


	public SignupPage() {

		PageFactory.initElements(driver, this);

	}	

	public void CreateUser(String firstname,String lastname,String email,String password,String confirmpwd) throws InterruptedException  {

		FirstName.sendKeys(firstname);

		LastName.sendKeys(lastname);

		EmailID.sendKeys(email);

		Password.sendKeys(password);

		Confirmpwd.sendKeys(confirmpwd);

		checkTOS.click();


		//signbt.click();

	}





	public void clicksignupbtn() throws InterruptedException  {

		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", Signupbtn);


		//w = new WebDriverWait(driver,40);

		Signupbtn.submit();
	}


	public void sendOTP(int num) throws InterruptedException {
		// TODO Auto-generated method stub
	
		s = String.valueOf(num);
		String str=driver.findElement(By.id("mat-dialog-0")).getText();
		verifybtn.submit();
		
		if (str.contains("Please enter the 6 digit code we just sent you at")) {
			
	
			
			box1.click();
			box1.sendKeys(s);
			Thread.sleep(500);
			verifybtn.submit();
			
		}
		
		
	  
		
	}

}










