package gleac.base;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.PageLoadStrategy;
import org.openqa.selenium.SearchContext;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeDriverService;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.paulhammant.ngwebdriver.NgWebDriver;

import gleac.util.Lib;
import gleac.util.TestUtil;
import io.github.bonigarcia.wdm.WebDriverManager;

public class TestBase implements TestUtil{

	public static WebDriver driver;
	public static Properties prop ;
	public static NgWebDriver ngWebDriver;
	public static JavascriptExecutor jsDriver;

	static ExtentReports extent;
	static ExtentTest logger;


	//	public static SearchContext getDriver() {
	//		return driver;
	//
	//
	//	}


	public TestBase() { //change the T to small



		try {
			prop =new Properties();
			FileInputStream ip=new FileInputStream(TestUtil.config);
			prop.load(ip);



		}
		catch(FileNotFoundException e) {
			e.printStackTrace();
		} catch(IOException e) {
			e.printStackTrace();
		}

	}

	@BeforeMethod
	public static void initialization() {

		ExtentHtmlReporter reporter=new ExtentHtmlReporter("/Users/gmx/Desktop/Automation/Gleac/Reports/Report.html");

		extent = new ExtentReports();

		extent.attachReporter(reporter);

		logger=extent.createTest("Minibenchmark"); 
		String browserName=prop.getProperty("browser");
		if(browserName.equalsIgnoreCase("chrome")) {
			//System.setProperty("webdriver.chrome.driver", TestUtil.chrome);
			ChromeOptions chromeOptions = new ChromeOptions();
			WebDriverManager.chromedriver().setup();
			// System.setProperty("webdriver.chrome.silentOutput", "true");
			System.setProperty(ChromeDriverService.CHROME_DRIVER_SILENT_OUTPUT_PROPERTY, "true");
			driver = new ChromeDriver(chromeOptions);
			jsDriver =(JavascriptExecutor)driver;
			ngWebDriver=new NgWebDriver(jsDriver);
			ngWebDriver.waitForAngularRequestsToFinish();
			chromeOptions.setPageLoadStrategy(PageLoadStrategy.EAGER);

		}
		else if(browserName.equalsIgnoreCase("firefox")) {
			System.setProperty("webdriver.gecko.driver", "/Users/rentsher/Desktop/AutomationMini-master/Gleac/drivers/geckodriver");
			driver = new FirefoxDriver();
		}

		driver.manage().window().maximize();
		driver.manage().deleteAllCookies();
		driver.manage().timeouts().pageLoadTimeout(TestUtil.PAGE_LOAD_TIMEOUT, TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(TestUtil.IMPLICIT_WAIT, TimeUnit.SECONDS);
		driver.get(prop.getProperty("url"));

	}


	@AfterMethod
	public static void tearDown(ITestResult result) throws IOException {


		if(result.getStatus()==ITestResult.FAILURE)
		{
			String temp=Lib.getScreenshot(driver);

			logger.fail(result.getThrowable().getMessage(), MediaEntityBuilder.createScreenCaptureFromPath(temp).build());
		}
		extent.flush();
		//		driver.quit();
		//driver.close();;
	}

}

